/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tmall.ultraviewpager;


import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;


public class UltraPagerMultiAdapter extends PageSliderProvider {
    private final Context context;
    private boolean isMultiScr;

    public UltraPagerMultiAdapter(Context context, boolean isMultiScr) {
        this.context=context;
        this.isMultiScr = isMultiScr;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        DirectionalLayout rootLayout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_layout_item_multi_pageslider, null,false);

        Text textView = (Text) rootLayout.findComponentById(ResourceTable.Id_pager_textview);
        textView.setText(position + "");

        ShapeElement background = new ShapeElement();
        rootLayout.setId(rootLayout.getId());
        switch (position) {
            case 0:
                background.setRgbColor(new RgbColor(33,150,243));
                rootLayout.setBackground(background);
                break;
            case 1:
                background.setRgbColor(new RgbColor(103,58,183));
                rootLayout.setBackground(background);
                break;
            case 2:
                background.setRgbColor(new RgbColor(0,150,136));
                rootLayout.setBackground(background);
                break;
            case 3:
                background.setRgbColor(new RgbColor(96,125,139));
                rootLayout.setBackground(background);
                break;
            case 4:
                background.setRgbColor(new RgbColor(244,67,54));
                rootLayout.setBackground(background);
                break;
        }
        componentContainer.addComponent(rootLayout);

        return rootLayout;
    }


    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int index, Object object) {
        DirectionalLayout view = (DirectionalLayout) object;
        componentContainer.removeComponent(view);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return component==object;
    }
}
