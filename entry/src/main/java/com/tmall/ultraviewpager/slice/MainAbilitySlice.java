/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tmall.ultraviewpager.slice;

import com.tmall.ultraviewpager.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.bundle.ElementName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 基础入口类
 */
public class MainAbilitySlice extends AbilitySlice {

    private List<Map<String, String>>   itemLists = new ArrayList<Map<String, String>>();;
    private ItemProvider itemProvider;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        ListContainer list = (ListContainer) findComponentById(ResourceTable.Id_list1);

        initData();

        itemProvider = new ItemProvider();
        list.setItemProvider(itemProvider);
        list.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i1, long l1) {
                Intent intent1= Intent.makeMainAbility(ElementName.createRelative("com.tmall.ultraviewpager","com.tmall.ultraviewpager.PageAbility",""));
                intent1.setParam("name", itemLists.get(i1).get("name"));
                intent1.setParam("style", itemLists.get(i1).get("style"));
                startAbility(intent1);
            }
        });
    }
class ItemProvider extends BaseItemProvider{
    @Override
    public int getCount() {

        return itemLists != null ? itemLists.size() : 0;
    }
    @Override
    public Object getItem(int i1) {

        return itemLists.get(i1).get("name");
    }

    @Override
    public long getItemId(int i1) {
        return i1;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        ViewHolder viewHolder = null;
        if (component == null) {
            component = LayoutScatter.getInstance(MainAbilitySlice.this).parse(ResourceTable.Layout_layout_child, null, false);
            viewHolder = new ViewHolder((ComponentContainer) component);
            component.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) component.getTag();
        }
        viewHolder.tvItemName.setText(itemLists.get(position).get("name"));
        return component;
    }

    class ViewHolder {
        Text tvItemName;
    ViewHolder(ComponentContainer componentContainer) {
            tvItemName = (Text) componentContainer.findComponentById(ResourceTable.Id_pager_textview);
        }
    }
}
    private void initData() {

        HashMap<String, String> demoList1 = new HashMap<>();
        demoList1.put("name", "Horizontal");
        demoList1.put("style", "1");
        itemLists.add(demoList1);

        HashMap<String, String> demoList2 = new HashMap<>();
        demoList2.put("name", "Vertical");
        demoList2.put("style", "2");
        itemLists.add(demoList2);


    }

    @Override
    public void onActive() {
        super.onActive();

    }



    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);

    }
}

