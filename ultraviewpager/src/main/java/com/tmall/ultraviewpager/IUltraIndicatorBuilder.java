/*
 *
 *  MIT License
 *
 *  Copyright (c) 2017 Alibaba Group
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 */

package com.tmall.ultraviewpager;


import ohos.media.image.PixelMap;


/**
 * 指示器构建类
 */
public interface IUltraIndicatorBuilder {
    /**
     * Set focused color for indicator.
     * @param focusColor 获得焦点颜色
     * @return 指示器构造类
     */
    IUltraIndicatorBuilder setFocusColor(int focusColor);

    /**
     * Set normal color for indicator.
     *
     * @param normalColor 正常指示器颜色
     * @return 指示器构造类
     */
    IUltraIndicatorBuilder setNormalColor(int normalColor);

    /**
     * Set stroke color for indicator.
     *
     * @param strokeColor 边线颜色
     * @return 指示器构造类
     */
    IUltraIndicatorBuilder setStrokeColor(int strokeColor);

    /**
     * Set stroke width for indicator.
     *
     * @param strokeWidth 边线宽度
     * @return 指示器构造类
     */
    IUltraIndicatorBuilder setStrokeWidth(int strokeWidth);

    /**
     * Set spacing between indicator item ，the default value is item's height.
     *
     * @param indicatorPadding 指示器间隔
     * @return 指示器构造类
     */
    IUltraIndicatorBuilder setIndicatorPadding(int indicatorPadding);

    /**
     * Set the corner radius of the indicator item.
     *
     * @param radius 半径
     * @return 指示器构造类
     */
    IUltraIndicatorBuilder setRadius(int radius);

    /**
     * Sets the orientation of the layout.
     *
     * @param orientation 指示器方向
     * @return 指示器构造类
     */
    IUltraIndicatorBuilder setOrientation(UltraViewPager.Orientation orientation);

    /**
     * Set the location at which the indicator should appear on the screen.
     *
     * @param gravity Gravity
     * @return 指示器构造类
     */
    IUltraIndicatorBuilder setGravity(int gravity);

    /**
     * Set focused resource ID for indicator.
     *
     * @param focusResId 获取焦点资源id
     * @return 指示器构造类
     */
    IUltraIndicatorBuilder setFocusResId(int focusResId);

    /**
     * Set normal resource ID for indicator.
     *
     * @param normalResId 普通资源id
     * @return 指示器构造类
     */
    IUltraIndicatorBuilder setNormalResId(int normalResId);

    /**
     * Set focused icon for indicator.
     *
     * @param bitmap 图片资源
     * @return 指示器构造类
     */
    IUltraIndicatorBuilder setFocusIcon(PixelMap bitmap);

    /**
     * Set normal icon for indicator.
     *
     * @param bitmap
     * @return 指示器构造类
     */
    IUltraIndicatorBuilder setNormalIcon(PixelMap bitmap);

    /**
     * Set margins for indicator.
     *
     * @param left   the left margin in pixels
     * @param top    the top margin in pixels
     * @param right  the right margin in pixels
     * @param bottom the bottom margin in pixels
     * @return 指示器构造类
     */
    IUltraIndicatorBuilder setMargin(int left, int top, int right, int bottom);

    /**
     * Combine all of the options that have been set and return a new IUltraIndicatorBuilder object.
     */
    void build();
}
